from django.urls import path
from django.conf.urls import url
from .import views

urlpatterns = [
    url(r'personalinfo$', views.user_info,),
]