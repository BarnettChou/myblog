from django.shortcuts import render

# Create your views here.
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.core import serializers
import json

@require_http_methods(["GET"])
def user_info(request):
    # 假資料
    context={}
    context['userName'] = 'Henry'
    context['userEex'] = 'man'
    context['userLive'] = 'Taiwan'
    context['userEmail'] = 'z7323056@'
    context['userPhon'] = '0910-549312'

    context1={}
    context1['userName'] = 'windows'
    context1['userEex'] = 'man'
    context1['userLive'] = 'USA'
    context1['userEmail'] = 'windows@'
    context1['userPhon'] = '0800-66-60-37'

    context2={}
    context2['userName'] = 'ios'
    context2['userEex'] = 'man'
    context2['userLive'] = 'USA'
    context2['userEmail'] = 'ios@'
    context2['userPhon'] = '0800-020-021'


    list = [context,context1,context2]
    encodedjson =  json.dumps(list)

    # return JsonResponse(list)
    return JsonResponse(list, safe=False)